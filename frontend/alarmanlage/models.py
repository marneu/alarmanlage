from django.urls import reverse
from django.db import models
from django.utils import timezone
# from pytz import timezone

# Create your models here.
class DOOR1(models.Model):
    time    = models.DateTimeField('time added')
    closed  = models.IntegerField(default=0,verbose_name='closed (yes=1, no=0)')

    def save(self, *args, **kwargs):
        super(DOOR1, self).save(*args, **kwargs)

class PIR1(models.Model):
    time     = models.DateTimeField('time added')
    movement = models.IntegerField(default=0,verbose_name='movement detected (yes=1)')

    def save(self, *args, **kwargs):
        super(PIR1, self).save(*args, **kwargs)

class CONFIG(models.Model):
    name    = models.TextField(default="")
    value   = models.TextField(default="")
    time    = models.DateTimeField('last modified')

    def save(self, *args, **kwargs):
        super(CONFIG, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.name
    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('config-detail', kwargs={'pk': self.pk})
