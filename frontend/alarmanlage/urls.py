from django.conf.urls import url
from . import views
# from .views import ConfigCreate, ConfigUpdate, ConfigDelete

app_name = 'alarmanlage'
urlpatterns = [
    url(r'^sensors/', views.SensorView.as_view(), name='sensors'),
    url(r'^stream/', views.StreamView.as_view(), name='stream'),
    # url(r'^config/', views.ConfigView.as_view(), name='config'),
    # url(r'config/add/$', ConfigCreate.as_view(), name='config-add'),
    # url(r'config/(?P<pk>[0-9]+)/$', ConfigUpdate.as_view(), name='config-update'),
    # url(r'config/(?P<pk>[0-9]+)/delete/$', ConfigDelete.as_view(), name='config-delete'),
    # url(r'config/(?P<pk>[0-9]+)/$', ArticleDetailView.as_view(), name='article-detail'),
]
