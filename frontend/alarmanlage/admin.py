from django.contrib import admin
from alarmanlage.models import *
from django import forms
from alarmanlage.forms import *

class CONFIGAdmin(admin.ModelAdmin):
    list_display = ('name', 'value', 'time')

    formfield_overrides = {
        models.TextField: {
            'widget': forms.Textarea(attrs={
                        'cols': '20',
                        'rows': '2'
                        })
        },
    }

class DoorAdmin(admin.ModelAdmin):
    list_display = ('closed', 'time')

class PirAdmin(admin.ModelAdmin):
    list_display = ('movement', 'time')


admin.site.register(DOOR1, DoorAdmin)
admin.site.register(PIR1, PirAdmin)
admin.site.register(CONFIG, CONFIGAdmin)
