from django.shortcuts import render
from django.http import HttpResponse
# from django.urls import reverse
from django.urls import reverse_lazy
from django.views import generic
from django.views.generic.edit import CreateView, UpdateView, DeleteView
# from django.utils import timezone
from django import forms
from django.shortcuts import redirect, render_to_response, get_object_or_404
from django.template import RequestContext
from .models import *
# from .forms import ConfigForm

# from django.db.models import Q

class StreamView(generic.ListView):
    template_name       = 'alarmanlage/stream.html'
    context_object_name = 'config'
    queryset            = CONFIG.objects.all()

class SensorView(generic.ListView):
    template_name = 'alarmanlage/sensors.html'
    context_object_name = 'sensor_list'

    queryset = DOOR1.objects.all()

    def get_context_data(self, **kwargs):
        context = super(SensorView, self).get_context_data(**kwargs)
        context['door1_last']   = DOOR1.objects.last()
        context['pir1_last']    = PIR1.objects.last()
        context['pir1']         = PIR1.objects.all()
        context['config']       = CONFIG.objects.all()
        return context

# class ConfigCreate(CreateView):
#     model = CONFIG
#     # fields = ['name','value','time']
#     success_url = '/sensors/'
#     form_class = ConfigForm
#
# class ConfigUpdate(UpdateView):
#     model = CONFIG
#     fields = ['name','value','time']
#     success_url = '/sensors/'
#
# class ConfigDelete(DeleteView):
#     model = CONFIG
#     success_url = '/sensors/'

# class ConfigView(generic.FormView):
#     template_name = 'alarmanlage/config.html'
#     form_class = ConfigForm
#     success_url = '/sensors/'
#
#     context_object_name = 'context'
#     # queryset = CONFIG.objects.all()
#
#     def get_context_data(self, **kwargs):
#         context = super(ConfigView, self).get_context_data(**kwargs)
#         context = {
#             'config':   CONFIG.objects.all(),
#             'form':     ConfigForm
#         }
#         return context
#
#     def form_valid(self, form):
#         # This method is called when valid form data has been POSTed.
#         # It should return an HttpResponse.
#         form.save()
#         return super(ConfigView, self).form_valid(form)
