#!/usr/bin/python3

import RPi.GPIO as GPIO
import time, datetime
import os, sys
import sqlite3

PROJECT_DIR = '/home/pi/ihk-projekt/' # /home/user/project_dir
DATABASE_PATH = PROJECT_DIR + 'alarm.db'
# address = "" # user@host
# passw   = ""

# if len(sys.argv) != 3:
#     print("\n\tusage:\tpython tĂźrkontakt.py address password \n\n\texample: python tĂźrkontakt.py 'hans@192.168.1.42' xyz\n")
#     raise SystemExit
# else:
#     address  = sys.argv[1]
#     password = sys.argv[2]


GPIO.setmode(GPIO.BCM) # GPIO Nummern statt Board Nummern

MAGNET_GPIO = 17
GPIO.setup(MAGNET_GPIO, GPIO.IN) # GPIO Modus zuweisen

current_state  = 0
previous_state = 0

def print_header():
    os.system("clear")
    print("\n\nTĂźrkontakt-Test (CTRL-C zum Beenden)")
    print("=========================================\n\n")

def log(closed):
    """logs to db; 'closed' has values 0 or 1, but is string"""
    conn = sqlite3.connect(DATABASE_PATH)
    print("Opened database successfully")
    time = datetime.datetime.utcnow()
    sql = "INSERT INTO alarmanlage_door1 (time,closed) \
          VALUES (\'" + str(time) + "\', " + closed + ")"
    conn.execute(sql)
    conn.commit()
    print("Records created successfully")
    conn.close()

def record(address, password, duration):
    os.system("sshpass -p %s ssh %s ffmpeg -f mjpeg \
    -i http://admin:admin@192.168.102.182/cgi/mjpg/mjpg.cgi -vcodec mpeg4 \
    -b 1000000 -t %i 'stream_$(date -Is).avi' -nostdin" \
    % (password, address, duration) )

try:
    while True:
        current_state = GPIO.input(MAGNET_GPIO)

        if current_state == 1 and previous_state == 0:
            print_header()
            print("%s: TĂźr ist geschlossen." % datetime.datetime.now())
            log("1")
            previous_state = 1

        elif  current_state == 0 and previous_state == 1:
            print_header()
            print("%s: Alarm, TĂźr ist offen!" % datetime.datetime.now())
            log("0")
            # os.system('echo "DOOR1 wurde geĂśffnet" | mail -s "alarm" deejott.orange@gmail.com')
        #    record(address, password, 10)
            previous_state = 0

        time.sleep(0.2)

except KeyboardInterrupt:
    print(" Exit")
    GPIO.cleanup()
