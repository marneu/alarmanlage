#!/usr/bin/python3

import sqlite3
import datetime

conn = sqlite3.connect('alarm.db')
print("Opened database successfully")

# sql = "INSERT INTO DOOR1 (TIME,CLOSED) \
#       VALUES (\'" + str(datetime.datetime.now()) + "\', 1)"
sql =   "INSERT INTO alarmanlage_config(time,name,value) \
        VALUES (\'" + str(datetime.datetime.utcnow()) + "\',\'alarm_on\',\'True\')"

conn.execute(sql)
conn.commit()
print("Records created successfully")
conn.close()
