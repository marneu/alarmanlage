#!/usr/bin/python3

# Import der Python libraries
import RPi.GPIO as GPIO
import time, datetime
import sqlite3
import os, sys

PROJECT_DIR = '/home/pi/ihk-projekt/' # /home/user/project_dir
DATABASE_PATH = PROJECT_DIR + 'alarm.db'
# address = "" # user@host
# passw   = ""

# if len(sys.argv) != 3:
#     print("\n\tusage:\tpython bewegungsmelder.py address password \
#     \n\n\texample: python bewegungsmelder.py 'hans@192.168.1.42' xyz\n")
#     raise SystemExit
# else:
#     address  = sys.argv[1]
#     password = sys.argv[2]

def log(movement):
    """logs to db; 'movement' has values 0 or 1, but is string"""
    conn = sqlite3.connect(DATABASE_PATH)
    print("Opened database successfully")
    time = datetime.datetime.utcnow()
    sql = "INSERT INTO alarmanlage_pir1 (TIME,MOVEMENT) \
          VALUES (\'" + str(time) + "\', " + movement + ")"
    conn.execute(sql)
    conn.commit()
    print("Records created successfully")
    conn.close()

def record(address, password, duration):
    os.system("sshpass -p %s ssh %s ffmpeg -f mjpeg \
    -i http://admin:admin@192.168.102.182/cgi/mjpg/mjpg.cgi -vcodec mpeg4 \
    -b 1000000 -t %i 'stream_$(date -Is).avi' -nostdin" \
    % (password, address, duration) )

#Board Mode, Angabe der PIN Nummern anstelle der GPIO BCM Nummer
GPIO.setmode(GPIO.BCM)
# GPIO definieren, 7 da bei mir der Sensor auf Pin7 steckt
GPIO_PIR = 4
#  GPIO als "Input" festlegen
GPIO.setup(GPIO_PIR,GPIO.IN)

print("Bewegungsmelder Test (CTRL-C zum Beenden)")
print("=========================================")

Current_State  = 0
Previous_State = 0

try:
    print("%s: Sensor initialisieren ..." % datetime.datetime.now())

    # Warten bis Sensor sich meldet
    while GPIO.input(GPIO_PIR)==1:
        Current_State  = 0

    print("%s: Fertig! Warte auf Bewegung..." % datetime.datetime.now())

    while True:
        Current_State = GPIO.input(GPIO_PIR)

        if Current_State==1 and Previous_State==0:
            # print("%s: Bewegung erkannt!" % datetime.datetime.now(pytz.timezone('Europe/Berlin')))
            print("%s: Bewegung erkannt!" % datetime.datetime.now())
            print(os.system('pwd'))
            log("1")
            # os.system('echo "PIR1 hat eine Bewegung erkannt!" | mail -s "alarm" deejott.orange@gmail.com')
        #    record(address, password, 10)
            Previous_State=1
            time.sleep(30.0)

        elif Current_State==0 and Previous_State==1:
            print("%s: Fertig! Warte auf Bewegung..."  % datetime.datetime.now())
            # log("0")
            Previous_State=0

        time.sleep(0.05)

except KeyboardInterrupt:
    print(" Exit")
    GPIO.cleanup()
