# **Alarmanlage**

Eine Alarmanlage auf Raspberry-Pi-Basis.

### **Anforderungen**

Host-Rechner: Raspberry Pi 2 oder Raspberry Pi 3  

IP-Cam: Allnet ALL2281

Bewegungsmelder: Foxnovo HC-SR501, Herstellerreferenz 14001355054-1,
ASIN B00PRFPY72

Türkontakt: EAN A14060400UX0169

Betriebssystem: Raspbian Jessie mit Kernel 4.4

Python-Version: >=3.4  

Django-Version: 1.10

### **automatisierte Installation**

    mkdir ~/alarmanlage && cd ~/alarmanlage

    git clone git@bitbucket.org:marneu/alarmanlage.git

    chmod -R +xr

    ./deploy.sh


### **manuelle Installation**

    mkdir ~/alarmanlage && cd ~/alarmanlage

    git clone git@bitbucket.org:marneu/alarmanlage.git

    chmod -R +xr

    sudo cp \*.service /etc/systemd/system  

    sudo systemctl daemon-reload  

    sudo systemctl enable bewegungsmelder tuerkontakt alarmanlagen-frontend

    sudo systemctl restart bewegungsmelder tuerkontakt alarmanlagen-frontend
