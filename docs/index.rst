.. Alarmanlage documentation master file, created by
   sphinx-quickstart on Thu May 11 23:23:12 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Alarmanlage's documentation!
=======================================

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   intro
   anford
   aufbau
   installation
   deploy
   bedienung




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
