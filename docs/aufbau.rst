=======================
**Aufbau der Hardware**
=======================

In diesem Teil wird beschrieben, wie die Hardware (RPi, Sensoren, IP-Cam) auf- und zusammengebaut wird.

- Schematiken/Schaltpläne (des Prototyps)

- Bilder (des Prototyps)
