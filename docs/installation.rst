================
**Installation**
================

**automatisierte Installation**
-------------------------------

- install.sh ins Arbeitsverzeichnis kopieren

- folgenden Code in der Befehlszeile ausführen:

.. code:: bash

    chmod +x install.sh
    ./install

Jetzt sollte die GUI im Webbrowser unter 0.0.0.0:3001/sensors erreichbar sein. Die Auswertungsskripte müssen in diesem Modus separat gestartet werden, siehe "Bedienung".


**manuelle Installation**
-------------------------

.. code:: bash

    #!/bin/bash

    # variables
    project_dir=~/alarmanlage
    envname=env
    project_name=frontend
    python_executable_path=$project_dir/$envname/bin/python
    app_name=alarmanlage
    AppName=Alarmanlage
    script_location=~/bin
    git_url=https://bitbucket.org/marneu/alarmanlage.git

    # install dependencies
    sudo apt-get install -q -y	-o Dpkg::Options::="--force-confdef" \
    						    -o Dpkg::Options::="--force-confold" \
    python3 python3-pip git

    git clone $git_url

    # enter project dir
    cd $project_dir

    # build virtualenv
    pip3 install virtualenv
    virtualenv -p python3 $envname
    source $envname/bin/activate # exit with 'deactivate'

    cd $project_name

    # install dependencies that are python-packages
    pip install -r requirements.txt

    # start django to test installation
    cd $project_dir
    source env/bin/activate
    python $project_name/manage.py runserver 0.0.0.0:3001 &
