===============
**Einleitung**
===============

Eine Alarmanlage auf Raspberry-Pi-Basis.

zu beschreibende Unterpunkte:

- Passwörter

- Anforderungen

- Aufbau der Hardware

- Installation der Software (im Entwicklungsmodus)

- Deployment der Software (Produktionsmodus)

- Aufbau der Software (-> UML, Datenmodell)

- Erläuterung der Methoden des Backends (log, record)

- Einbindung der Projektdokumentation (für IHK)

- Bedienung des Frontends

-
