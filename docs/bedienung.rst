=============
**Bedienung**
=============

In diesem Bereich wird die Bedienung erläutert. Nicht Gegenstand: Installation, Einrichtung des Betriebssystems, Einrichtung des Netzwerks.

-----------------------------
**Starten der CLI-Anwendung**
-----------------------------
Sowohl die Befehlszeilenanwendung als auch die grafische Benutzeroberfläche starten automatisch beim Hochfahren des Betriebssystems.

----------------------------
**Zugriff auf Webanwendung**
----------------------------
Im Webbrowser folgende Adresse eingeben:
rpi/sensors
“rpi” steht für die IP-Adresse des Raspberry Pi; beim Anwender wird es sich wahrscheinlich um eine 192.168.x.x-Adresse handeln.

---------------
**Unterseiten**
---------------

**Sensoren**
------------
Hier werden die Inhalte der Datenbank-Tabellen in einem kurzen Überblick ausgegeben. Interessant ist insbesondere der aktuelle Zustand der Sensoren.

**Livestream der IP-Cam**
-------------------------
Wenn die Alarmanlage scharfgestellt ist, wird hier der Livestream der IP-Cam ausgegeben, d.h. man sieht mit ~2 Sekunden Verzögerung, was die Kamera gerade sieht. Ist die Alarmanlage nicht scharfgestellt, wird der Stream nicht angezeigt.
Die Scharfstellung wird in der Admin-Oberfläche vorgenommen.

**Admin-Oberfläche**
--------------------
Hier gelangt man zur Admin-Oberfläche, für die man sich zuerst mit Benutzername und Passwort anmelden muss.
In diesem Bereich gibt es u.a. folgende Optionen:

- Home › Authentication and Authorization › Users
	- Benutzer anlegen und löschen
	- Benutzerdaten und -rechte ändern
- Home › Authentication and Authorization › Groups
	- Gruppen anlegen und löschen, z.B. für normale Nutzer, die Schreibzugriff auf 	die Datentabellen haben, nicht jedoch auf die Gruppen- und Nutzertabellen
	- Benutzerdaten und -rechte ändern
- Home › Alarmanlage › Configs
	- Konfigurationen anlegen und löschen
	- Konfigurationen ändern

Zur Zeit vorhandene Werte der Konfigurationstabelle:

emails:
	Hier sind die Email-Adressen kommasepariert aufgelistet, an die im Falle 	eines 	Alarms Benachrichtigungen verschickt werden.
alarm_on:
	Kann die Werte “True” oder “False” annehmen. “True” bedeutet, dass die 	Alarmanlage scharfgestellt ist. Durch ändern dieses Wertes wird die 	Alarmanlage scharfgestellt bzw. die Scharfstellung aufgehoben.
