=================
**Anforderungen**
=================

Host-Rechner: Raspberry Pi 2 oder Raspberry Pi 3

IP-Cam: Allnet ALL2281

Bewegungsmelder: Foxnovo HC-SR501, Herstellerreferenz 14001355054-1,
ASIN B00PRFPY72

Türkontakt: EAN A14060400UX0169

Betriebssystem: Raspbian Jessie mit Kernel 4.4

Python-Version: >=3.4

Django-Version: 1.10
