#!/bin/bash

# This script will document, and possibly automate, how to install this project
# with virtualenv.
# operating system(s): debian 8

# variables
project_dir=~/alarmanlage
envname=env
project_name=frontend
python_executable_path=$project_dir/$envname/bin/python
app_name=alarmanlage
AppName=Alarmanlage
script_location=~/bin
git_url=https://bitbucket.org/marneu/alarmanlage.git
#user=

# install dependencies
sudo apt-get install -q -y	-o Dpkg::Options::="--force-confdef" \
						    -o Dpkg::Options::="--force-confold" \
python3 python3-pip git

git clone $git_url

# enter project dir
cd $project_dir

# build virtualenv
pip3 install virtualenv
virtualenv -p python3 $envname
source $envname/bin/activate # exit with 'deactivate'

cd $project_name

# install dependencies that are python-packages
pip install -r requirements.txt
pwd

# start django
cd $project_dir
source env/bin/activate
python $project_name/manage.py runserver 0.0.0.0:3001 &

echo 'Fertig. GUI sollte im Browser unter 0.0.0.0:3001/sensors erreichbar sein.'
