#!/bin/bash

# This script will document, and possibly automate, how to deploy this project
# with virtualenv and apache.
# operating system(s): debian 8
#
# referenced howto: https://www.digitalocean.com/community/tutorials/how-to-run-django-with-mod_wsgi-and-apache-with-a-virtualenv-python-environment-on-a-debian-vps

# see project 'schimpf'


# ------ install project ------------------------------------------------------

# variables
project_dir=~/alarmanlage
project_name=frontend
app_name=alarmanlage
AppName=Alarmanlage
env_dir=$project_dir/env
script_location=~/bin
git_url=https://bitbucket.org/marneu/alarmanlage.git

# install dependencies
sudo apt-get install -q -y	-o Dpkg::Options::="--force-confdef" \
						    -o Dpkg::Options::="--force-confold" \
apache2 libapache2-mod-wsgi-py3 python3 python3-pip git

# pull project code from git
if [ $project_dir/$project_name ]
then echo "found project directory"
else git clone $git_url
fi

cd $project_dir

# build virtualenv
if [ -f $env_dir/bin/python ]
then echo "found virtualenv-dir"
else
	echo "installing virtualenv" \
	pip3 install virtualenv \
	virtualenv -p python3 $env_dir
fi
source $env_dir/bin/activate # exit with 'deactivate'

cd $project_name

# install dependencies that are python-packages
pip install -r requirements.txt
pwd

# --- configure project for deployment (static files, permissions) ------------

# # whitenoise: lass django die statischen dateien selbst behandeln:
# # siehe https://www.webforefront.com/django/staticwhitenoise.html (printversion
# # um nicht von blockadblock genervt zu werden)
pip install whitenoise
rm requirements.txt
pip freeze > requirements.txt
pwd

#add the 'whitenoise.middleware.WhiteNoiseMiddleware' middleware class to your
# settings.py file in the second position -- after 'SecurityMiddleware'
#python3
cat <<HEK>> code.py
import fileinput
line_present = False
for line in fileinput.FileInput('${project_name}/settings.py',inplace=1):
    if 'whitenoise.middleware.WhiteNoiseMiddleware' in line:
        line_present = True
    print(line, end=''),
if line_present == False:
    for line in fileinput.FileInput('${project_name}/settings.py',inplace=1):
        if "'django.middleware.security.SecurityMiddleware'," in line:
            line=line.replace(line,line + """\t'whitenoise.middleware.WhiteNoiseMiddleware',\n""")
        print(line, end=''),
print("python code ran successfully.")
HEK
python code.py && rm code.py

# jetzt noch STATIC_ROOT in settings.py definieren:
cat <<HEK>> $project_name/settings.py
"STATIC_ROOT = '${project_dir}/${project_name}/static/'\n")
HEK
#
# # statische dateien sammeln
cd ${project_dir}/${project_name} && python manage.py collectstatic
# # migration / datenbank erzeugen
cd ${project_dir}/${project_name} && python manage.py migrate
#
# # interne verlinkungen funktionieren leider nicht gut ...
# # in Templates:
# # <link rel="stylesheet" type="text/css" href="{% static 'style.css' %}" />
# # # ersetzen mit:
# # <link rel="stylesheet" type="text/css" href="static/style.css" />

# wsgi.py
cat <<HEK>> $project_name/wsgi.py
"""
WSGI config for schimpf project.
It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.10/howto/deployment/wsgi/
"""
import os
from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "schimpf.settings")
application = get_wsgi_application()
HEK

# # ----------- configure apache ----------------------------------------------

# # apache conf bearbeiten aka 000-default.conf
cat <<HEK>> code.py
import fileinput
for line in fileinput.FileInput('/etc/apache2/sites-enabled/000-default.conf',inplace=1):
    if "<VirtualHost *:80>" in line:
        line=line.replace(line,line +
"""WSGIDaemonProcess schimpf python-path=${project_dir}/${project_name}:/usr/local/lib/python3.4/dist-packages
WSGIProcessGroup ${project_name}
WSGIScriptAlias ${project_name} ${project_dir}/${project_name}/${project_name}/wsgi.py
<Location ${project_name}>
	 Allow from all
</Location>
""")
    print(line, end=''),
HEK
sudo python code.py && sudo rm code.py

# # httpd.conf aka apache2.conf
sudo cat <<EOT>> /etc/apache2.conf
WSGIScriptAlias ${project_name} ${project_dir}/${project_name}/${project_name}/wsgi.py
WSGIPythonPath /home/rynnon/schimpf/schimpf

<Directory ${project_dir}/${project_name}/${project_name}>
	<Files wsgi.py>
		Require all granted
	</Files>
</Directory>
EOT

# # apache die berechtigung geben, auf die datenbank zuzugreifen
# cd /home/rynnon/workspace-pi
# chmod -R 777 schimpf
# # # alternativ und besser:
# # # siehe: http://stackoverflow.com/questions/31631731/use-a-git-repository-on-var-www-html
sudo chown -R pi:www-data $project_dir && chmod -R g+sw $project_dir
# # apache (re)starten
sudo service apache2 restart
# # logs
# tail /var/log/apache2/error.log
# tail /var/log/apache2/access.log

echo 'Fertig. Bitte Templates manuell anpassen wie im Skript beschrieben.'
