#!/usr/bin/python3

import sqlite3

conn = sqlite3.connect('alarm.db')
print("Opened database successfully")

# conn.execute('''CREATE TABLE DOOR1
#        (ID INTEGER PRIMARY KEY,
#        TIME     TEXT    NOT NULL,
#        CLOSED   INT     NOT NULL);''')
# conn.execute('''CREATE TABLE PIR1
#        (ID INTEGER PRIMARY KEY,
#        TIME       TEXT    NOT NULL,
#        MOVEMENT   INT     NOT NULL);''')
conn.execute('''CREATE TABLE CONFIG
       (ID          INTEGER PRIMARY KEY,
       NAME         TEXT    NOT NULL,
       VALUE        TEXT    NOT NULL,
       LAST_UPDATED TEXT    NOT NULL);''')


print("Table created successfully")

conn.close()
