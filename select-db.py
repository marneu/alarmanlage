#!/usr/bin/python3

import sqlite3

conn = sqlite3.connect('alarm.db')
print("Opened database successfully")

cursor = conn.execute("SELECT id, time, closed from alarmanlage_door1")
for row in cursor:
   print("ID = ", row[0])
   print("TIME = ", row[1])
   print("CLOSED = ", row[2], "\n")

# cursor = conn.execute("SELECT id, time, movement from PIR1")
# for row in cursor:
#    print("ID = ", row[0])
#    print("TIME = ", row[1])
#    print("MOVEMENT = ", row[2], "\n")

# cursor = conn.execute("SELECT id, name, value, LAST_UPDATED FROM config")
# for row in cursor:
#    print("ID = ", row[0])
#    print("name = ", row[1])
#    print("value = ", row[2])
#    print("last_updated = ", row[3], "\n")

print("Operation done successfully")
conn.close()
